import React, { Component } from 'react';
import BattlePlayerCard from './BattlePlayerCard';
import { useParams, useSearchParams, Link } from 'react-router-dom';

class BattleResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDataFetched: false
        };
    }
    getWinner = () => {
        const score_player_1 = this.state.player_1.data.followers * 3 + this.state.player_1.data.public_repos / 2;
        const score_player_2 = this.state.player_2.data.followers * 3 + this.state.player_2.data.public_repos / 2;
        let winner;
        let loser;
        if(score_player_1 > score_player_2){
            winner = {
                data: this.state.player_1,
                score: score_player_1,
                status: 'Winner'
            };
            loser = {
                data: this.state.player_2,
                score: score_player_2,
                status: 'Loser'
            };
        }
        else if(score_player_1 < score_player_2){
            winner = {
                data: this.state.player_2,
                score: score_player_2,
                status: 'Winner'
            };
            loser = {
                data: this.state.player_1,
                score: score_player_1,
                status: 'Loser'
            };
        }
        else{
            winner = {
                data: this.state.player_1,
                score: score_player_1,
                status: 'Tie'
            };
            loser = {
                data: this.state.player_2,
                score: score_player_2,
                status: 'Tie'
            };
        }
        return {winner, loser};
    };
    fetchBattleData = (player_1, player_2) => {
        [player_1, player_2].forEach((player, index) => {
            fetch(`https://api.github.com/users/${player}`)
                .then(data => data.json())
                .then(data => {
                    const playerType = `player_${index + 1}`; 
                    this.setState({
                        [playerType]: {
                            data
                        },
                        isDataFetched: index === 1 ? true : false
                    });
                });
        });
    };

    componentDidMount() {  
        const queryParams = new URLSearchParams(window.location.search);
        this.fetchBattleData(queryParams.get("playerOne"), queryParams.get("playerTwo"));
    }
    componentWillUnmount() {
        this.setState({
            isDataFetched: false
        });
    }
    
    render() {
        if(!this.state.isDataFetched){
            return <h3> Fetching Results </h3>
        }
        const {winner, loser} = this.getWinner();
        return ( 
            <section>
                    <div className='battle-player-container'>
                        <BattlePlayerCard player={winner} isDarkTheme={this.props.isDarkTheme}/>
                        <BattlePlayerCard player={loser} isDarkTheme={this.props.isDarkTheme}/>
                    </div>
                    <Link to='/battle' className='link'>
                        <button type='reset' className='submit-btn' onClick={this.props.resetBattle}> RESET </button>
                    </Link> 
            </section>
         );
    }
}

export default BattleResult;