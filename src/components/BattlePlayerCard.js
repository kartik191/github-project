import React, { Component } from 'react';
import userIcon from '../images/user-icon.svg';
import locationIcon from '../images/location-icon.svg';
import followersIcon from '../images/followers-icon.svg';
import followingIcon from '../images/following-icon.svg';
import codeIcon from '../images/code-icon.svg';

class BattlePlayerCard extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        const {data} = this.props.player.data;
        return ( 
            <article className={this.props.isDarkTheme ? 'battle-player-card card-dark' : 'battle-player-card'}>
                <section className='card-battle-top'>
                    <h2 className='rank'> {this.props.player.status} </h2>
                    <img src={data.avatar_url} alt='username-image' className='username-image'/>
                    <span className='score'> Score: {this.props.player.score} </span>
                </section>
                <a href={data.html_url} className='repo-url-battle'> {data.login} </a>
                <section>
                    <div className='card-info-row'>
                        <img src={userIcon} alt='user'/>
                        <span className='card-info'> {data.name} </span>
                    </div>
                    <div className='card-info-row' style={data.location == null ? {display: 'none'} : {}}>
                        <img src={locationIcon} alt='location'/>
                        <span className='card-info'> {data.location} </span>
                    </div>
                    <div className='card-info-row'>
                        <img src={followersIcon} alt='followers'/>
                        <span className='card-info'>  {data.followers} followers</span>
                    </div>
                    <div className='card-info-row'>
                        <img src={followingIcon} alt='following'/>
                        <span className='card-info'> {data.following} following</span>
                    </div>
                    <div className='card-info-row'>
                        <img src={codeIcon} alt='repos'/>
                        <span className='card-info'> {data.public_repos} repositories</span>
                    </div>
                </section>
            </article>
         );
    }
}
 
export default BattlePlayerCard;