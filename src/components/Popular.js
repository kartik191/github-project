import React, { Component } from 'react';
import PopularCard from './PopularCard';

class Popular extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {},
          language: 'all'
        };
    }  
    languageChange = (language) => {
      this.setState({
        language
      });
    }     
    fetchData(){
      const language = this.state.language;
      if(this.state.data[language] === undefined){
        const api_url = `https://api.github.com/search/repositories?q=stars:%3E1+language:${language}&sort=stars&order=desc&type=Repositories`;
        fetch(api_url)
          .then(res => res.json())
          .then(data => {
            data = data.items;
            let languagesData = {...this.state.data};
            languagesData[language] = {         
              data,
              isfetched: true
            }; 
            this.setState({
              data: languagesData
            })
          }); 
      }
    }

    componentDidMount(){
      this.fetchData();
    }
    componentDidUpdate(){
      this.fetchData();
    }
    render() {
      const {language} = this.state;
      const headerText = 'header-text';
      const headerSelectedText = 'header-text header-text-selected';
      return (
        <main>
          <header className='header-popular'>
            <span className={language === 'all' ? headerSelectedText : headerText} onClick={() => this.languageChange('all')}> All </span>
            <span className={language === 'javascript' ? headerSelectedText : headerText} onClick={() => this.languageChange('javascript')}> JavaScript </span>
            <span className={language === 'ruby' ? headerSelectedText : headerText} onClick={() => this.languageChange('ruby')}> Ruby </span>
            <span className={language === 'java' ? headerSelectedText : headerText} onClick={() => this.languageChange('java')}> Java </span>
            <span className={language === 'css' ? headerSelectedText : headerText} onClick={() => this.languageChange('css')}> CSS </span>
            <span className={language === 'python' ? headerSelectedText : headerText} onClick={() => this.languageChange('python')}> Python </span>
          </header>
          <section className='container-popular'>
            {
              this.state.data[language] === undefined ? 
                <h3 className='fetching-data'> Fetching Repos </h3> :
                this.state.data[language].data.map((user, index) => <PopularCard key={user.name} user={user} rank={index + 1} isDarkTheme={this.props.isDarkTheme}/>)
            }
          </section>
        </main>
        );
    }
}
 
export default Popular;