import React, { Component } from 'react';
import PlayerCard from './PlayerCard';
import PlayerForum from './PlayerForum';

class Player extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( 
            <article>
                <h2 className='player-heading'> Player {this.props.playerType === 'player_1' ? 'One' : 'two'} </h2>                    
                <div>
                    {
                        this.props.player.isAvailable ? 
                            <PlayerCard removePlayer={this.props.removePlayer} playerType={this.props.playerType} player={this.props.player} isDarkTheme={this.props.isDarkTheme}/> : 
                            <PlayerForum setPlayer={this.props.setPlayer} playerType={this.props.playerType} isDarkTheme={this.props.isDarkTheme}/>
                    }
                </div>
            </article>
         );
    }
}
 
export default Player;