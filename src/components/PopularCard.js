import React, { Component } from 'react';
import profileIcon from '../images/profile-icon.svg';
import starIcon from '../images/star-icon.svg'
import issueIcon from '../images/issue-icon.svg'
import forkIcon from '../images/git-icon.svg'

class PopularCard extends Component {
    constructor(props) {
        super(props);
    }
    formatNumber = number => {
        return number.toString().split('').reduce((prev, curr, index, array) => {
            if(index > 0 && (array.length - index) % 3 === 0){
                return prev + ','  + curr;
            }
            return prev + curr;
        }, '');
    };
    render() {
        const {user, isDarkTheme} = this.props;
        return ( 
            <article className={isDarkTheme ? 'card-popular card-dark' : 'card-popular'}>
                <section className='container-popular-top'>
                    <h2 className='rank'> #{this.props.rank} </h2>
                    <img src={user.owner.avatar_url} alt='username-image' className='username-image'/>
                    <a href={user.html_url} className='repo-url'> {user.owner.login} </a>
                </section>
                <section className='card-popular-info'>
                    <div className='card-info-row'>
                        <img src={profileIcon} alt="profile"/>
                        <a href={user.owner.html_url} className='card-info username'> {user.owner.login}</a>
                    </div>
                    <div className='card-info-row'>
                        <img src={starIcon} alt="star"/>
                        <span className='card-info'> {this.formatNumber(user.watchers_count)} stars</span>
                    </div>
                    <div className='card-info-row'>
                        <img src={forkIcon} alt="fork"/>
                        <span className='card-info'> {this.formatNumber(user.forks_count)} forks</span>
                    </div>
                    <div className='card-info-row'>
                        <img src={issueIcon} alt="issue"/>
                        <span className='card-info'> {this.formatNumber(user.open_issues_count)} open issues</span>
                    </div>
                </section>
            </article>
         );
    }
}
 
export default PopularCard;