import React, { Component } from 'react';
import Player from './Player';
import userImage from '../images/users.svg';
import jetImage from '../images/jet.svg';
import winnerImage from '../images/winner.svg';
import {Link} from 'react-router-dom';

class BattleIntro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            player_1: {
                isAvailable: false 
            },
            player_2: {
                isAvailable: false
            }
        };
    }
    removePlayer = (playerType) => {
        this.setState({
            [playerType]: {
                isAvailable: false
            }
        });
    }
    setPlayer = (playerType, username) => {
        this.setState({
            [playerType]: {
                isAvailable: true,
                username
            }
        });
    };
    render() {
        const {player_1, player_2} = this.state;
        return (  
            <article>
                <section className='battle-intro'>
                    <h2 className='battle-title'> Instructions </h2>
                    <section className='battle-intro-text'>
                        <div>
                            <h5 className='icon-header'> Enter two Github users </h5>
                            <img src={userImage} alt='users'/>
                        </div>
                        <div>
                            <h5 className='icon-header'> Battle </h5>
                            <img src={jetImage} alt='jet'/>
                        </div>
                        <div>
                            <h5 className='icon-header'> See the winner  </h5>
                            <img src={winnerImage} alt='winner'/>
                        </div>
                    </section>
                </section>
                <section>
                    <h2 className='battle-title'> Players </h2>
                    <div className='players-container'>
                        <Player player={player_1} playerType={'player_1'} isDarkTheme={this.props.isDarkTheme} setPlayer={this.setPlayer} removePlayer={this.removePlayer}/>
                        <Player player={player_2} playerType={'player_2'} isDarkTheme={this.props.isDarkTheme} setPlayer={this.setPlayer} removePlayer={this.removePlayer}/>
                    </div>
                   <Link to={`/battle/results?playerOne=${player_1.username}&playerTwo=${player_2.username}`} className='link'>
                        <button type='button' className='submit-btn' style={player_1.isAvailable && player_2.isAvailable ? {display: 'inline'} : {display:'none'}}> BATTLE</button>
                   </Link> 
                </section>
            </article>
        );
    }
}
 
export default BattleIntro;