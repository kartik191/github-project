import React, { Component } from 'react';

class PlayerForum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ''
        };
    }
    inputChange = (event) => {
        this.setState({
            input: event.target.value
        });
    }
    submit = (event) => {
        event.preventDefault();
        if(this.state.input.trim() !== ''){
            this.props.setPlayer(this.props.playerType, this.state.input);
            this.setState({
                input: ''
            });
        }
    };
    render() { 
        const darkClass = this.props.isDarkTheme ? 'btn-dark' : '';
        return (  
            <form onSubmit={this.submit}>
                    <input type='text' placeholder='github username' className={this.props.isDarkTheme ? 'input-username input-dark' : 'input-username'} value={this.state.input} onChange={this.inputChange}/>
                    <button type='submit' className={this.state.input ? `submit-btn ${darkClass}` : 'submit-btn submit-btn-disable'} disabled={this.state.input ? '' : 'disabled'}> SUBMIT </button>
            </form>
        );
    }
}
 
export default PlayerForum;