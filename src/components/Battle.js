import React, { Component } from 'react';
import { Route, Routes } from 'react-router-dom';
import BattleIntro from './BattleIntro';
import BattleResult from './BattleResult';

class Battle extends Component {
    constructor(props) {
        super(props);
    }    
    render() {
        return ( 
            <main className='battle'>

                <Route exact path='/battle'> <BattleIntro isDarkTheme={this.props.isDarkTheme}/> </Route>
                <Route path='/battle/results'> <BattleResult isDarkTheme={this.props.isDarkTheme}/> </Route>
            </main>
        );
    }
}
 
export default Battle;