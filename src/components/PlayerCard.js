import React, { Component } from 'react';
import crossIcon from '../images/cross-icon.svg';

class PlayerCard extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        const {username} = this.props.player;
        return ( 
            <article className={this.props.isDarkTheme ? 'player-card player-dark' : 'player-card'}>
                <section className='user-info'>
                    <img src={`https://github.com/${username}.png?size=200`} alt='user-icon' className='user-icon'/>
                    <a href={`https://github.com/${username}`} className='card-username'> {username} </a>
                </section>
                <img src={crossIcon} alt='X' onClick={() => this.props.removePlayer(this.props.playerType)}/>
            </article>
         );
    }
}
 
export default PlayerCard;