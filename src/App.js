import React, { Component } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Popular from './components/Popular';
import Battle from './components/Battle';
import './App.css';
import darkLogo from './images/dark-theme.svg';
import lightLogo from './images/light-theme.svg';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPopular: true,
      isDarkTheme: false
    };
  }

  changeMain = (value) => {
    this.setState({
      isPopular: value
    });
  }
  changeTheme = () => {
    this.setState({
      isDarkTheme: !this.state.isDarkTheme
    });
  }
  render() {
    const {isPopular, isDarkTheme} = this.state;
    return (
      <div className={isDarkTheme ? 'app  app-dark' : 'app'}>
        <BrowserRouter>
          <header className='header-top'>
            <Link to='/' className={isPopular ? 'header-text header-text-selected' : 'header-text'} onClick={() => this.changeMain(true)}> Popular </Link>
            <Link to='/battle' className={!isPopular ? 'header-text header-text-selected' : 'header-text'} onClick={() => this.changeMain(false)}> Battle </Link>
            <img src={isDarkTheme ? lightLogo : darkLogo} alt='theme-icon' className='theme-icon' onClick={this.changeTheme}/>
          </header>
          
          <Route exact path='/'> <Popular isDarkTheme={isDarkTheme}/> </Route>
          <Route path='/battle'> <Battle isDarkTheme={isDarkTheme}/> </Route>
        </BrowserRouter>
      </div>
     );
  }
}

export default App;
